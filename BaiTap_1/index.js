function sapXep() {
  var number1 = document.getElementById("txt-number1").value * 1;
  var number2 = document.getElementById("txt-number2").value * 1;
  var number3 = document.getElementById("txt-number3").value * 1;
  var T1 = 0;
  var T2 = 0;
  var T3 = 0;
  if ((number1 < number2) & (number1 < number3)) {
    T1 = number1;
  } else if ((number2 < number1) & (number2 < number3)) {
    T1 = number2;
  } else {
    T1 = number3;
  }
  if ((number1 > number2) & (number1 > number3)) {
    T3 = number1;
  } else if ((number2 > number1) & (number2 > number3)) {
    T3 = number2;
  } else {
    T3 = number3;
  }
  if (
    (number1 > number2) & (number1 < number3) ||
    (number1 > number3) & (number1 < number2)
  ) {
    T2 = number1;
  } else if (
    (number2 > number1) & (number2 < number3) ||
    (number2 > number3) & (number2 < number1)
  ) {
    T2 = number2;
  } else {
    T2 = number3;
  }
  document.getElementById("result").innerHTML = `${T1}, ${T2}, ${T3}`;
}
